import React from 'react';
import Course from '../components/Course';

import Container from 'react-bootstrap/Container';

// Data import
import courses from '../mock-data/courses'

export default function Courses(){
	const CourseCards = courses.map((course) => {
		return (
				<Course course = {course} />
			)
	})
	return (
			<Container fluid>
				{CourseCards}
			</Container>
		)
}