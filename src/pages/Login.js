import React, {useState, useEffect} from 'react';
import {Form, Container, Button} from 'react-bootstrap';

export default function Login(){
	const [email,setEmail] = useState('')
	const [password,setPassword] = useState('')
	const [isDisabled,setIsDisabled] = useState(true)

	useEffect(() => {
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';
		
		if (isEmailNotEmpty && isPasswordNotEmpty) {
			setIsDisabled(false)
		}else {
			setIsDisabled(true)
		}
	}, [email, password])

	function login(e){
		e.preventDefault();
		alert ('You are now logged in!')

		setEmail('')
		setPassword('')
	}

	return(
			<Container>
				<Form onSubmit={login}>
					<h1>Login</h1>
					<Form.Group>
						<Form.Label>Email Address</Form.Label>
						<Form.Control type='email' placeholder='Enter email' value={email} onChange={(e) => setEmail(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type='password' placeholder='Password' value={password} onChange={(e) => setPassword(e.target.value)} required />
					</Form.Group>
					<Button variant='primary' type='submit' disabled={isDisabled}>Login</Button>
				</Form>
			</Container>
		)
}