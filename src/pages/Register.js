import React, {useState, useEffect} from 'react';
import {Form, Container, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'

export default function Register(){
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [passwordConfirm, setPasswordConfirm] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(() => {
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';
		let isPasswordConfirmNotEmpty = passwordConfirm !== '';
		let isPasswordMatch = password === passwordConfirm;

		if(isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	},[email, password, passwordConfirm])

	function register(e){
		e.preventDefault();
		Swal.fire('Register successful, you may now log in.')

		setEmail('');
		setPassword('');
		setPasswordConfirm('');
	}

	return(
		<Container>
			<Form onSubmit={register}>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
					<Form.Text className="text-muted">We'll never share your email to anyone else</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)}required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm password</Form.Label>
					<Form.Control type="password" placeholder="Confirm password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required />
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
			</Form>
		</Container>
		)

}