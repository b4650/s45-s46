import {React, useState} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import AppNavbar from './components/AppNavbar.js';

import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';

export default function App() {
	const [user, setUser] = useState(null)

	return (
			<Router>
			  <AppNavbar user={user} />
			  <Routes>
			  <Route path='/' element = {<Home/>} />
			  <Route path='/courses' element = {<Courses/>} />
			  <Route path='/register' element = {<Register/>} />
			  <Route path='/login' element = {<Login/>} />
			  </Routes>
			</Router>
		)
}