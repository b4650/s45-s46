/*
  after installing, remove all files inside the src folder except index.js
  remove the README.md file in the root folder of the app
*/

import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import App from './App.js'

// App component
/*
  every component has to be imported before it can be rendered
*/

/*
  Fragment - used to render different components inside the index.js. Without it, the webpage will return errors since, it is the of the JS to display two or more components in the frontend
  <>
  </>
  - also accepted in place of the Fragment but not all browsers are able to read this. also this does not support keys and attributes
*/

ReactDOM.render(
  <App />
,
document.getElementById('root'));