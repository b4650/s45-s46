import React, {useState, useEffect} from 'react';

import Container from 'react-bootstrap/container'

export default function Counter() {
	const [count, setCount] = useState(0)

	useEffect(() => {
		document.title = `You clicked ${count} times`;
	},[])


	return(
			<Container fluid>
				<p>You clicked {count} times</p>
				<button onClick={() => setCount(count+1)}>Click me!</button>
			</Container>
		)
}