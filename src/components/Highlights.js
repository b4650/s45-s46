import React from 'react';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Highlights(){
	return(
			<Row>
				<Col xs={12} md={4}>
					<Card className="card-highlight">
						<Card.Body>
							<Card.Title>
								<h2>Learn From Home</h2>
							</Card.Title>
							<Card.Text>
								Enjoy learning with the comfort of your home
							</Card.Text>
						</Card.Body>					
					</Card>
				</Col>
				
				<Col xs={12} md={4}>
					<Card className="card-highlight">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>
								Never be bothered by the expenses while learning
							</Card.Text>
						</Card.Body>					
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="card-highlight">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of our Community</h2>
							</Card.Title>
							<Card.Text>
								Zuitt offers job for aspiring web developers from beginners to experienced who want to refresh their mind in coding
							</Card.Text>
						</Card.Body>					
					</Card>
				</Col>
			</Row>
		)
}