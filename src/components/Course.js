import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

export default function Courses(props){
	let course = props.course
	const [isDisabled, setIsDisabled] = useState(0)
	const [seat, setSeat] = useState(30)

/*	function enroll(){
		if(seat === 0) {
			alert ("No more seats.")
		} else {
			setCount (count + 1)
			seatCount (seat - 1)
		}
	}*/

	useEffect(() => {
		if (seat === 0) {
			setIsDisabled(true)
		}
	},[seat])

	return(
			<Row>
				<Col>
					<Card style={{marginTop: "1rem"}}>
						<Card.Body>
							<Card.Title>{course.name}</Card.Title>
							<Card.Text>
								<h6>Description</h6>
								<p>{course.description}</p>
								<h6>Price</h6>
								<p>{course.price}</p>
								<h6>Seats</h6>
								<p>{seat} remaining</p>
							</Card.Text>
							<Button variant="primary" onClick={() => setSeat(seat-1)} disabled={isDisabled}>Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
} 